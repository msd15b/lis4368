> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Matthew Spenser Dubin

### Assignment 5 Requirements:

*Three Parts:*

1. Valid Entry Form
2. Success of Entry form with filled in fields
3. Associated Database in MySQL

### README.md file should include the following items:

* Screenshot of valid entry form (#1 above)
* Screenshot of running success entry form (#2 above)
* Screenshot of associated database with customer entry (#3 above)


#### Project  Screenshots:

*Screenshot of Valid Entry Form*:

![Entry Screenshot](img/val_entry_form.png)

*Screenshot of Passed Valid Entry Form*:

![Entry Success Screenshot](img/passed_val.png)

*Screenshot of Associated Database Entry*

![Associated Database Screenshot](img/as_db_entry.png)

