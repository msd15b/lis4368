> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Matthew Spenser Dubin

### Assignment 1 Requirements:

*Three Parts:*

1. Distrubuted Version Control With Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running http://localhost:9999 (#2 above);
* git commands with short descriptions
* Bitbucket repo links a) assignment and b) tutorials

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1.  git init - create new local repository
2.  git status - List the files you've changed and those you still need to add or commit
3.  git add - Add one or more files to staging (index)
4.  git commit - Stores the current contents of the index in a new commit along with a log message from the user
5.  git push - Send changes to the master branch of your remote repository
6.  git pull - Fetch and merge changes on the remote server to your working directory
7.  git branch - List all the branches in your repo, and also tell you what branch you're currently in

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![AMPPS Installation Screenshot](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
