> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Matthew Spenser Dubin

### Project 1 Requirements:

*Two Parts:*

1. Failed JSP with empty fields
2. Success JSP with filled in fields
3. Modified Home page

### README.md file should include the following items:

* Screenshot of running JSP results (#2 above)
* Screenshot of running JSP failed (#1 above)
* Screenshot of Home page with new images (#3 above)


#### Project  Screenshots:

*Screenshot of JSP Failed Page results*:

![JSP Screenshot](img/fail_page.png)

*Screenshot of JSP Success Page results*:

![JSP Success Screenshot](img/success_page.png)

*Screenshot of Tomcat Homepage*:

![Home Page Screenshot](img/home_page.png)


