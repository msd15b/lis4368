# LIS4368 - Advanced Web Application Development

## Matthew Spenser Dubin

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
        (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Develop and Deploy Java Servlets
    - Provide screenshots of deployed Servlets
    - Push to Bitbucket repo
    - Complete Java Servlet and MySQl tutorial
        (Develop and Deploy a WebApp)

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD)
    - Include data (at least 10 records each table)
    - Push to Bitbucket repo
    
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Failed Field JSP Page
    - Success Field JSP Page
    - Modified Home Page
    - Push to Bitbucket repo
    
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Failed JSP without input data
    - Success JSP with filled in fields
    - Modified url links

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Valid Entry Form
    - Success of Entry form with filled in fields
    - Associated Database in MySQL

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Passed validation form
    - Display data form
    - Modify data form
    - Modified data table form
    - Deletion from table warning
    - MySQL database changes (Select, Insert, Update, Delete)