> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Matthew Spenser Dubin

### Assignment 2 Requirements:

*Three Parts:*

1. Distrubuted Version Control With Git and Bitbucket
2. MySQL Installation and Java Servlet Development
3. Chapter Questions (5-6)

### README.md file should include the following items:

* Screenshot of running query results (#2 above);

#### Assignment Screenshots:

*Screenshot of running query results*:

![Query Results Screenshot](img/query_results.png)

#### Assessment Links:

1. [A2 hello](http://localhost:9999/hello "Hello Directory")
2. [A2 HelloHome](http://localhost:9999/hello/HelloHome.html "This is my Home")
3. [A2 SayHello](http://localhost:9999/hello/sayhello "Hello World")
4. [A2 QueryBook](http://localhost:9999/hello/querybook.html "Query Book")
5. [A2 SayHi](http://localhost:9999/hello/sayhi "Hello World, Again")
