## LIS4368 - Advanced Web Applications Development

## Matthew Spenser Dubin

### Project 2 Requirements:

*Seven Parts:*

1. Passed validation form
2. Display data form
3. Modify data form
4. Modified data table form
5. Deletion from table warning
6. MySQL database changes (Select, Insert, Update, Delete)

### README.md file should include the following items:

* Screenshot of valid user entry form
* Screenshot of passed validation entry form (#1 above)
* Screenshot of displayed data form (#2 above)
* Screenshot of the modify data form (#3 above)
* Screenshot of the modified data table form (#4 above)
* Screenshot of deletion from table warning (#5 above)
* Screenshot of associated database changes (#6 above)


#### Project  Screenshots:

*Screenshot of Valid Entry Form*:

![Entry Screenshot](img/valEntryForm.png)

*Screenshot of Passed Valid Entry Form*:

![Entry Success Screenshot](img/passedVal.png)

*Screenshot of Display Data*

![Display Data Screenshot](img/displayData.png)

*Screenshot of Modify Form*

![Modify Form Screenshot](img/modForm.png)

*Screenshot of Modified Data*

![Modified Data Screenshot](img/modData.png)

*Screenshot of Delete Warning*

![Deletion Warning Screenshot](img/delWarn.png)

*Screenshot of Associated Database Changes (Select, Insert, Update, Delete)*

![Associated Database Screenshot](img/asDB.png)

